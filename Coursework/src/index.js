
const renderArray = [];
const comments = [];
let messageId = 0;

class Post {
    constructor(messageId, author, date, text, imageLink, imagePreview, likes, comments) {
        this.id = messageId;
        this.author = author;
        this.date = date;
        this.text = text;
        this.imageLink = imageLink;
        this.imagePreview = imagePreview;
        this.likes = 0;

        renderArray.push(this);

        localStorage.setItem('post', JSON.stringify(renderArray));
        let message_list = document.getElementById('message_list');
    }

    render() {

        this.newLi = document.createElement('li');
        this.newLi.innerHTML = `
                    <div class="post__author"></div>
                        <span class="span_author"><b>${this.author}</b></span>
                    <div class="post_date">
                        <span><b>${this.date}</b></span>
                    </div>
                    <div class="post__text">
                         <span class="span_text">${this.text}</span>
                    </div>
                        <div class="post_likes">
                    <button class="button">Likes ${this.likes}</button> 
                        </div>
                       <div class="comments_comments">
                      <span class="span"></span>
                    </div>
                  
                    `;

        message_list.appendChild(this.newLi);

        let buttonHTMLCollection = this.newLi.getElementsByClassName('button');
        let button = Array.from(buttonHTMLCollection);

        button.forEach((item) => {
            item.addEventListener('click', () => {
                this.likes += 1;
                item.innerHTML = `Likes ${this.likes}`
            });
        })
    }
}

const messageForm = document.getElementById('message');
let authors = document.getElementById('author');
let textArea = messageForm.elements.message;
let date = new Date();
let i = 0;

        const RenderFeed  = ( arr )  => {
           arr.map ( item => {
               item.render()
           })
        };

        RenderFeed( renderArray );

messageForm.addEventListener('submit', (e) => {
    e.preventDefault();
    let author = authors.value;
    let text = textArea.value;
    let post_Date = () => {return `${date.getDate()}/${1 + date.getMonth()}/${date.getFullYear()}`};

    let post = new Post (messageId++, author, post_Date(), text, comments);
    post.render();
});

    class Comment extends Post {
        constructor (messageId, author, date, text) {
            super(messageId, author, date, text);
            this.parentId = messageId;


            this.newDivAnswer = document.createElement("div");
            this.newDivAnswer.classList.add('comments');

        }

        renderAnswer() {
            let comments_comments_list = document.querySelectorAll('.comments_comments'); //NodeList
            let comments_comments_array = Array.prototype.slice.call(comments_comments_list);

            this.newDivAnswer.innerHTML = `
                    <div class="comment__date">
                      <span class="span_comment__date">${this.date}</span>
                    </div>
                    <div class="comment__author">
                        <span class="span_comment__author"><b>${this.author}</b></span>
                    </div>

                    <div class="comment__text">
                        <span class="span_comment__text">${this.text}</span>
                    </div>
                    `;
            comments_comments_array[messageId - 1].appendChild(this.newDivAnswer)
        }
    }

const commentForm = document.getElementById("comment");
let authorsComment = document.getElementById('comment_author');
let textAreaComment = commentForm.elements.comment_text;
let dateComment = new Date();

commentForm.addEventListener('submit', (e) => {
    e.preventDefault();

    let text = textAreaComment.value;
    let author = authorsComment.value;
    let date = () => {return `${dateComment.getDate()}/${1 + dateComment.getMonth()}/${dateComment.getFullYear()}`};

    let answer = new Comment( messageId, author, date(), text);
    answer.renderAnswer();
    console.log(answer);
})





